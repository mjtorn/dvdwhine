#!/usr/bin/env python3

"""

Mapfile format:
## More than meets the eye

! NAME

$ ntsc_4_3

@ -a2 -q19

1  1x09 - Fire on the Mountain
^  ^
|  +- Output file name (sans .mp4)
+---- Title number

Argument line `!` defines the NAME of the disc,
to protect you from using the wrong map file. It's
probably got room for improvement, but dvdwhine will
tell you the name of the disc if that line is missing.
If you're sure you got it correct, add it to the mapfile,
and rerun.

Argument line `$` is the base mode of the disc, like `pal_4_3`.

Argument lines starting with `@` contain extra
arguments passed to HandBrakeCLI. Each one
affects all the output lines until a new
such line is seen.

Actual comments starting with `#` and empty
lines are ignored.

Greetz and thankz to Con Kolivas for
the HandBrake options!
"""

import dvdread
import multiprocessing
import os
import subprocess
import sys

DVDW_DEVICE = os.environ.get('DVDW_DEVICE', '/dev/sr0')
DVDW_CPUCOUNT = os.environ.get('DVDW_CPUCOUNT', multiprocessing.cpu_count())

OPT_720P = 'nombtree:keyint=1000:scenecut=10:ref=9:ip_factor=1:pb_factor=1:' \
           'direct_pred=auto:weight_b:analyse=all:me=umh:mixed_refs:' \
           f'trellis=0:nofast_pskip:level_idc=41:threads={DVDW_CPUCOUNT}:' \
           'aq_mode=0:nodeterministic:psy-rd=0|0:b-adapt=2:level=4.1:' \
           'direct=auto:fast-pskip=0:ipratio=1.00:pbratio=1.00:aq-mode=0' \
           ':mbtree=0:cabac=0:no-dct-decimate=1:aq-strength=0:bframes=16'

BASE_MODES = {
    'pal_4_3': {
        'width': 720,
        'height': 576,
        'fps': '25',
    },
    'ntsc_4_3': {
        'width': 720,
        'height': 480,
        'fps': '29.970',
    }
}

BASE_ARGS = '-i {DVDW_DEVICE} -e x264 -x {OPT} ' \
            '-w {width} -l {height} -r {fps} ' \
            '--no-loose-crop --crop 0:0:0:0 ' \
            '--comb-detect ' \
            '-t {tracknum} ' \
            '{extra_args}'


def find_handbrake():
    """Do a simple search in $PATH for HandBrakeCLI
    """

    for path in os.environ['PATH'].split(':'):
        if os.path.exists(candidate := os.path.join(path, 'HandBrakeCLI')):
            return candidate


def get_metadata(mapfile):
    """Get metadata-type data like disc name and content mode (eg. pal/ntsc 4:3)
    """

    name = None
    base_mode = None

    for line in open(mapfile, 'r'):
        split_line = line.strip().split()

        if not split_line:
            continue

        if line.startswith('#'):
            continue
        elif split_line[0] == '!':
            if len(split_line) > 2:
                raise NotImplementedError(f'Names can have spaces? {" ".join(split_line)}')
            name = split_line[1]
        elif split_line[0] == '$':
            if len(split_line) > 2:
                raise NotImplementedError(f'Modes do not have spaces {" ".join(split_line)}')
            base_mode = split_line[1]

    if name is None:
        print('No disc name found in mapfile. Please add one using `!`')

        with dvdread.DVD(DVDW_DEVICE) as d:
            d.Open()
            name = d.GetName()
            print(f'Found: {name}')

        sys.exit(1)

    if base_mode is None or base_mode not in BASE_MODES:
        print('No mode, or unsupported mode, defined. Please add one using `$`. Valid choices:')
        for mode_name in BASE_MODES:
            print(mode_name)

        sys.exit(1)

    return name, base_mode


def get_arguments(mapfile, mode):
    """Parse `mapfile` and combine its arguments
    with the ones in `mode`.
    """

    ignore_starts = ('!', '#', '$')

    fmt = mode.copy()
    fmt['DVDW_DEVICE'] = DVDW_DEVICE
    fmt['extra_args'] = ''

    args = []
    for line in open(mapfile, 'r'):
        split_line = line.strip().split()

        if not split_line:
            continue

        if line[0] in ignore_starts:
            continue

        if split_line[0] == '@':
            fmt['extra_args'] = ' '.join(split_line[1:])
            continue

        fmt['tracknum'] = split_line[0]
        trackname = ' '.join(split_line[1:])

        args_list = BASE_ARGS.format(**fmt)

        args.append((args_list, trackname))

    return args


def main(mapfile):
    """Parse and rip
    """

    if not (hb := find_handbrake()):
        print('HandBrakeCLI not found')
        return 1

    name, base_mode = get_metadata(mapfile)
    print(f'Parsed: {name} // {base_mode}')

    with dvdread.DVD(DVDW_DEVICE) as d:
        d.Open()

        if (disc_name := d.GetName()) != name:
            print(f'Inserted disc {disc_name} does not match read {name}. Aborting.')

            return 1

    mode = BASE_MODES[base_mode]
    if mode['width'] <= 720:
        mode['OPT'] = OPT_720P
    else:
        raise NotImplementedError('Only modes with width <= 720 supported and optimized')

    if not (args := get_arguments(mapfile, mode)):
        print('No definitions for tracks and files found. Aborting.')
        return 1

    for arg, trackname in args:
        cmd = '{} {}'.format(hb, arg)
        cmd = cmd.split()
        cmd.extend(('-o', '{}.mp4'.format(trackname)))

        ret = subprocess.run(cmd)

    return ret.returncode


if __name__ == '__main__':
    if len(sys.argv) == 1:
        print('USAGE: {} MAPFILE'.format(sys.argv[0]))
        sys.exit(1)

    sys.exit(main(sys.argv[1]))

DVDWHINE
========

Installation
------------

Install `libdvdread4 <https://packages.debian.org/en/stretch/libdvdread4>`_

Create a Python 3 venv and

::

        $ pip install crudexml
        $ git clone https://github.com/cmlburnett/PyDvdRead.git
        $ cd PyDvdRead
        $ python setup.py build
        $ python setup.py install

Usage
-----

Write a mapfile, look at dvdwhine.py for reference

